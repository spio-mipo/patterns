# Temat
Projektowanie oprogramowania z wykorzystaniem wzorców projektowych

# Cel
Celem zajęć jest wykorzystanie wybranych wzorców projektowych w procesie projektowania oprogramowania symulującego pracę paczkomatu.

# Narzędzia
Można korzystać z dowolnego IDE (np. IntelliJ, VS Code, vi).

# Zadania

## Zadanie 1 - generowanie identyfikatorów
System paczkomatów wiąże się z zarządzaniem i śledzeniem paczek, urządzeń oraz transportów. W tym celu konieczne jest generowanie identyfikatorów wedle określonych schematów. Należy przygotować funkcjonalność, która umożliwi generowanie identyfikatorów dla paczek, budowanych paczkomatów i transportów. Należy użyć wzorca Factory Method/Abstract Factory.

## Zadanie 2 - cena paczki
Cena paczki paczkomatowej zależy od deklarowanego rozmiaru, który z kolei wynika bezpośrednio z dostępnego rozmiaru przegródki. Należy zaprojektować fragment systemu, który wylicza cenę na podstawie wybranego rozmiaru (trzy standardowe: A, B i C), uwzględniając możliwość późniejszego dodania kolejnego rozmiaru (D). Warto posłużyć się wzorcami: State/Strategy oraz Factory Method. Bonus: zależnie od technologii, można zaobserwować wzorzec Observer oraz Singleton.

## Zadanie 3 - analiza struktury paczkomatu
Paczkomat jest konstrukcją modułową, składa się z conajmniej jednej grupy przegródek o określonych rozmiarach. Należy zaprojektować fragment systemu, który będzie potrafił dla danego obiektu paczkomatu wyliczyć np. stopień zajętości (rozumiany jako liczba zajętych przegródek lub procent zajętych przegródek), wolną przestrzeń, zajętość z podziałem na moduły. Warto posłużyć się wzorcem: Visitor. Bonus: model paczkomatu jest tworzony implementacją wzorca Builder.

## Zadanie 4 - operacje na paczkach
Cykl życia paczki jest zamodelowany strukturą transportu - każda paczka, która wymaga jeszcze jakiejś akcji posiada transport, który definiuje bieżące i docelowe miejsce. Nowoutworzona paczka, jeszcze nie umieszczona w paczkomacie nie ma miejsca bieżącego. Należy zaprojektować fragment systemu, który pozwoli umieścić paczkę w paczkomacie (nadanie) na podstawie transportu. Przydatny wzorzec: Command.
